/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.huerta.oop.sosafe;

import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gohucan
 */
public class DenunciaTest {
    
    private Faker faker;
    private List<Denuncia> listaDenuncias;
    private Denuncia aBuscar;
    
    public DenunciaTest() {
    }
    
    @Before
    public void setUp() {
        this.faker = new Faker();
        this.listaDenuncias = new ArrayList<>(500);
        for(int i=0; i<500; i++) {
            String ubicacion = this.faker.address().fullAddress();
            String descripcion = this.faker.lorem().paragraph();
            int prioridad = this.faker.number().numberBetween(1, 5);
            TipoDenuncia tipo = this.faker.options().option(TipoDenuncia.class);
            Denuncia d = new Denuncia(ubicacion, descripcion, prioridad, tipo);
            if (tipo == TipoDenuncia.ROBO) {
                this.aBuscar = d;
            }
            this.listaDenuncias.add(d);
        }
    }
    
    @After
    public void tearDown() {
        this.listaDenuncias.clear();
    }

    @Test
    public void testOrdenaTipo() {
        Collections.sort(this.listaDenuncias, new Denuncia.ComparadorTipo());
        for(int i=0; i<499; i++) {
            assert this.listaDenuncias.get(i).getTipo().ordinal() <= this.listaDenuncias.get(i+1).getTipo().ordinal();
        }
        for(int i=0; i<499; i++) {
            System.out.println(this.listaDenuncias.get(i));
        }
    }

    @Test
    public void testOrdenaFecha() {
        Collections.sort(this.listaDenuncias, new Denuncia.ComparadorFecha());
        for(int i=0; i<499; i++) {
            assert this.listaDenuncias.get(i).getFechaHora().getTime() <= this.listaDenuncias.get(i+1).getFechaHora().getTime();
        }
        for(int i=0; i<499; i++) {
            System.out.println(this.listaDenuncias.get(i));
        }
    }
    
    @Test
    public void testBusqueda() {
        assert this.listaDenuncias.indexOf(this.aBuscar) > -1;
    }
    
}
